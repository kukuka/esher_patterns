from tkinter import Tk, Canvas
from math import cos, sin, pi

# ----------------------------Global var
n = 4  # number periods
dash = ((1, 1), (1, 1), (1, 1))
d = n * len(dash[0])  # Число повторений на экране
angle = 60
baz = ((1, 0), (cos(angle * pi / 180), sin(angle * pi / 180)), (1 - cos(angle * pi / 180), -sin(angle * pi / 180)))
l = 60*(1.3**7)
bazl = list(map((lambda x: [x[0] * l, x[1] * l]), baz))
W = 1300
H = 700
m_vec = [0, 0]  # move vector вектор смещения "картинки"
coords = [[0, 0, bazl[0][0], bazl[0][1]], [0, 0, bazl[1][0], bazl[1][1]],
          [0, 0, -bazl[1][0] + bazl[0][0],-bazl[1][1] + bazl[0][1]]]
#0.0, 0.0, 20.19466119999947, -16.201186399999983, 30.19466119999947, -39.20118639999997, 13.194661199999473, -61.201186399999976, 5.194661199999471,
#-102.20118639999997, -15.805338800000527, -48.201186399999976, -42.80533880000052, -57.20118639999998, -73.80533880000053, -58.201186399999976,
#-25.80533880000052, -123.20118639999998, -4.805338800000527, -171.20118639999998, 34.19466119999947, -152.20118639999998, 38.19466119999947,
#-121.20118639999998, 51.19466119999947, -95.20118639999998, 72.19466119999947, -79.20118639999997, 94.19466119999944, -88.20118639999997, 112.19466119999946,
#-101.20118639999997, 123.19466119999944, -116.20118639999998, 134.19466119999944, -133.20118639999995, 137.19466119999944, -159.20118639999998, 135.19466119999947,
#-186.20118639999998, 123.19466119999944, -209.20118639999993, 110.19466119999947, -227.20118639999995, 83.19466119999946, -243.20118639999998, 103.19466119999947,
#-270.2011864, 111.19466119999947, -300.2011864, 115.19466119999947, -334.2011864, 154.19466119999947, -354.2011864, 188.24555099999998, -326.05085863079825]]


coord_tag = [{}, {}, {}]
tags = [(0,),(0,),(0,)]
colors = ('GRAY', 'BLACK', 'BLACK', 'BLACK', 'PURPLE', 'CYAN', 'BLUE',
          'BLACK', 'PURPLE',
          'BLACK')  # 0-background 123-lines 4-edit_line 5-edit_line(active) 6-point 7-outline(point) 8-edpoint 9 - outline
size_p = 4  # px
sc_const = 1
num_draw = [0, 1, 2]  # m - local


# ----------------------------
def shift(A, B):  # A - массив точек (последовательно идут - xyxyxy, кратный двум)
    return (list(map(lambda X: X[0] + X[1], zip(A, B * (len(A) // 2)))))


def mirror_m(M, mir_p):  # mir_p - крайняя точка на которую нужно сместить
    a = list(map(lambda x: x * -1, M))
    b = [0] * len(a)
    for i in list(range(len(a)))[-1::-2]:
        if i != len(a):
            b[i - 1] = (a[len(a) - i - 1])
            b[i] = (a[len(a) - i])
    return (shift(b, mir_p))  # Выход координат


def line_gen(m):  # Генерирует массив(по заданным номерам) линий m - массив номеров линий которые нужно генерировать
    T = [[], [], []]
    lines = [[], [], []]
    for i in m:
        k = 0
        for j in dash[i]:
            if j == 1:
                T[i] += shift(coords[i], [bazl[i][0] * k, bazl[i][1] * k])  # без [:-2] ???
            elif j == -1:
                T[i] += shift(mirror_m(coords[i], bazl[i]), [bazl[i][0] * k, bazl[i][1] * k])  # без [:-2] ???
            k += 1
        for v in range(n):
            lines[i] += shift(T[i], [bazl[i][0] * v * len(dash[i]), bazl[i][1] * v * len(dash[i]) + H / 2])
    return (lines)  # Выводит массив линий (точек)


class picture():
    def __init__(self, flag):
        self.flag = flag

    def d(self, lines,
          m):  # Рисует заданные линии(по номерам из массива m), также создает линии для редактирования(тоже заданные)
        coords2 = [[], [], []]
        p = [1, 2, 1]  # Порядок направлений
        color1 = colors[1:4]
        for i in m:
            lines[i] = shift(lines[i], [m_vec[0], m_vec[1]])
            coords2[i] = shift(coords[i], [len(dash[0]) * l + m_vec[0], m_vec[1] + H / 2])
            if i != 0:
                for j in range(d + 1):
                    canv.create_line(shift(lines[i], [j * bazl[p[i]][0], j * bazl[p[i]][1]]), fill=color1[i],
                                     tag='lines' + str(i + 1))
            else:
                for j in range(d * 2 + 1):
                    canv.create_line(
                        shift(lines[0], [(j - (d + 1)) * abs(bazl[1][0]), (j - (d + 1)) * abs(bazl[1][1])]),
                        fill=color1[i], tag='lines1')
            # Линии редактирования
            canv.create_line(coords2[i],
                             tag='line_ed' + str(i + 1), fill=colors[4])
            canv.create_line(coords2[i],
                             tag='line_inv' + str(i + 1), fill='', width=13)
            # print(coords2)
            if self.flag == 'start':
                global tags
                for v in range(0, len(coords2[i]), 2):
                    # print(coords2[i][v], coords2[i][v + 1])
                    canv.create_rectangle(coords2[i][v] - size_p, coords2[i][v + 1] - size_p, coords2[i][v] + size_p,
                                          coords2[i][v + 1] + size_p,
                                          tag='point' + str(i + 1),
                                          fill='',
                                          outline='')
                    tags[i] = canv.find_withtag('point' + str(i + 1))            
        for i in m:
            canv.lift('line_ed' + str(i + 1))
        for i in m:
            canv.lift('point' + str(i + 1))
        for i in m:
            canv.lift('line_inv' + str(i + 1))


def teleport(event):  # Перемещение по "изображению"
    global m_vec
    global tags
    edit_canv.sel_var = None
    xo, yo = event.x, event.y
    m_vec[0], m_vec[1] = m_vec[0] + W / 2 - xo, m_vec[1] + H / 2 - yo
    canv.delete('all')
    strtdrw.d(line_gen(num_draw), num_draw)


def zoom(event, k):  # Масштабирование
    global sc_const
    global l
    global bazl
    global coords
    global m_vec
    global tags
    edit_canv.sel_var = None
    for i in range(3):
        bazl[i] = list(map(lambda x: x * k, bazl[i]))
    for i in range(3):
        coords[i] = list(map(lambda x: x * k, coords[i]))
    m_vec = list(map(lambda x: x * k, m_vec))
    m_vec[0] += (1 - k) * W / 2
    l *= k
    sc_const *= k
    canv.delete('all')
    strtdrw.d(line_gen(num_draw), num_draw)


def addp(event):
    global coords
    #global coord_tag
    global tags
    if edit_canv.sel_var != None:
        i = edit_canv.sel_var - 1
        xo, yo = event.x, event.y
        coords[i] = coords[i][:-2] + [xo - len(dash[0]) * l - m_vec[0], yo - m_vec[1] - H / 2] + coords[i][-2:]
        # coords[i] += [xo, yo] Метод добавления по направлению вектора
        # proj = []
        # for v in range(0, len(coords[i]), 2):
        #    proj.append(coords[i][v]*baz[i][0] + coords[i][v + 1]*baz[i][1])
        # d = {proj[v//2]: v for v in range(0, len(coords[i]), 2)}
        # proj.sort()
        # coords_copy = coords[:]
        # for v in range(0, len(coords[i]), 2):
        #     coords[i][v], coords[i][v+1] = coords_copy[i][d[proj[v//2]]], coords_copy[i][d[proj[v//2]]+1]
        canv.delete('lines' + str(i + 1), 'line_ed' + str(i + 1), 'line_inv' + str(i + 1))
        canv.create_rectangle(xo - size_p, yo - size_p, xo + size_p,
                              yo + size_p,
                              tag='point' + str(i + 1),
                              fill=colors[6],
                              outline=colors[7])
        tags[i] = canv.find_withtag('point' + str(i + 1))
        tags[i] = (tags[i][0],) + tags[i][2:] + (tags[i][1],)
        #coord_tag[i] = dict(zip(tags, [0] * len(tags)))
        #for j in range(len(coords[i]), 2):
        #    coord_tag[i][tags[j // 2]] = [coords[i][j], coords[i][j + 1]]
        drw.d(line_gen([i]), [i])
        canv.itemconfig('line_ed' + str(i + 1), fill=colors[5])
        canv.lift('point' + str(i + 1))



def test(event):
    i = edit_canv.sel_var - 1
    tag = list(coord_tag[i].keys())[-1]
    canv.itemconfig(tag, fill='BLACK')


def move(event):
    #global coord_tag
    global coords
    if edit_canv.tp != None:
        print(edit_canv.tp, tags)
        i = edit_canv.sel_var - 1
        xo, yo = event.x, event.y
        canv.coords(edit_canv.tp, xo - size_p, yo - size_p, xo + size_p, yo + size_p)
        #coord_tag[i][edit_canv.tp] = [xo - len(dash[0]) * l - m_vec[0], yo - m_vec[1] - H / 2]  # +-
        coords[i][tags[i].index(edit_canv.tp[0])*2], coords[i][tags[i].index(edit_canv.tp[0])*2+1] = xo - len(dash[0]) * l - m_vec[0], yo - m_vec[1] - H / 2
        canv.delete('lines' + str(i + 1), 'line_ed' + str(i + 1), 'line_inv' + str(i + 1))
        drw.d(line_gen([i]), [i])
        canv.itemconfig('line_ed' + str(i + 1), fill=colors[5])
        canv.lift('point' + str(i + 1))

class edit_canv:
    sel_var = None
    tp = None

    def __init__(self, number):
        self.number = number

    def select_line(self, event):
        if edit_canv.sel_var != self.number:
            canv.itemconfig('line_ed' + str(edit_canv.sel_var), fill=colors[4])
            canv.itemconfig('point' + str(edit_canv.sel_var), fill='', outline='')
            edit_canv.sel_var = self.number
            canv.itemconfig('line_ed' + str(self.number), fill=colors[5])
            canv.itemconfig('point' + str(self.number), fill=colors[6], outline=colors[7])
            canv.lift('point' + str(self.number))
        else:
            canv.itemconfig('line_ed' + str(self.number), fill=colors[4])
            canv.itemconfig('point' + str(edit_canv.sel_var), fill='', outline='')
            edit_canv.sel_var = None
            edit_canv.tp = None

    def select_pnt(self, event):
        if self.number == edit_canv.sel_var:
            if edit_canv.tp != canv.find_withtag('current'):
                canv.itemconfig('point' + str(edit_canv.sel_var), fill=colors[6], outline=colors[7])
                edit_canv.tp = canv.find_withtag('current')
                canv.itemconfig(edit_canv.tp, fill=colors[8], outline=colors[9])
            else:
                canv.itemconfig('point' + str(edit_canv.sel_var), fill=colors[6], outline=colors[7])
                edit_canv.tp = None


line1 = edit_canv(1)
line2 = edit_canv(2)
line3 = edit_canv(3)

strtdrw = picture('start')
drw = picture('pnt')

# ----------------------------Canvas
root = Tk()
canv = Canvas(root, width=W, height=H, bg=colors[0])
strtdrw.d(line_gen(num_draw), num_draw)
canv.focus_set()
canv.bind('<Shift-Double-Button-1>', teleport)
root.bind('<Up>', lambda event: zoom(event, 1.3))
root.bind('<Down>', lambda event: zoom(event, 1 / 1.3))
root.bind('<Return>', test)
canv.tag_bind('line_inv1', '<Button-3>', line1.select_line)
canv.tag_bind('line_inv2', '<Button-3>', line2.select_line)
canv.tag_bind('line_inv3', '<Button-3>', line3.select_line)
canv.tag_bind('point1', '<Button-1>', line1.select_pnt)
canv.tag_bind('point2', '<Button-1>', line2.select_pnt)
canv.tag_bind('point3', '<Button-1>', line3.select_pnt)
canv.bind('<Button-2>', addp)
canv.bind('<Shift-B1-Motion>', move)
canv.pack()
root.mainloop()
# ----------------------------Draw
