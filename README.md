# esher_patterns

Старая программа замощения плоскости (типа векторного редактора) требующая развития.

_[Работа программы видео](https://gitlab.com/kukuka/esher_patterns/-/blob/master/screenshots/preview.mp4)_

_[Работа художника М.К. Эшера](https://gitlab.com/kukuka/esher_patterns/-/blob/master/screenshots/esher.jpg)_

_[Скриншоты](https://gitlab.com/kukuka/esher_patterns/-/blob/master/screenshots/)_